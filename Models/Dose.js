import { DataTypes } from "sequelize";
import sequelize from "../Config/sequelize.js";

const Dose = sequelize.define(
  "Dose",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
  },
  {
    tableName: "doses",
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);
(async () => {
  try {
    await sequelize.sync();
    console.log("All models were synchronized successfully.");
  } catch (error) {
    console.error("Unable to synchronize the database:", error);
  }
})();
export default Dose;
