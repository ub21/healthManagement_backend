import { DataTypes } from "sequelize";
import sequelize from "../Config/sequelize.js";

const Reminder = sequelize.define(
  "Reminder",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    medication_type_id: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    dose: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    medication_schedule: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    medication_schedule_specific_week: {
      type: DataTypes.ARRAY(DataTypes.STRING),
      allowNull: false,
      defaultValue: [],
    },
    no_of_days: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    medication_start_date: {
      type: DataTypes.DATEONLY,
      allowNull: true,
    },
    medication_end_date: {
      type: DataTypes.DATEONLY,
      allowNull: true,
    },
    message: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    subscription: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    schedule_name: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    instructions: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    status: {
      type: DataTypes.ENUM('active', 'inactive'),
      defaultValue: 'active'
    },
  },
  {
    tableName: "reminders",
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default Reminder;
