import { DataTypes } from "sequelize";
import sequelize from "../Config/sequelize.js";
import User from "./User.js";

const Family = sequelize.define(
  "Family",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    profile_photo: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    relation: {
      type: DataTypes.ENUM,
      values: [
        "Father",
        "Mother",
        "Son",
        "Daughter",
        "Grandfather",
        "Grandmother",
        "Brother",
        "Sister",
        "Uncle",
        "Aunt",
        "Others",
      ],
      allowNull: false,
    },
    customRelation: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    gender: {
      type: DataTypes.ENUM,
      values: ["Male", "Female"],
      allowNull: false,
    },
    aadharNumber: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    patient_id: {
      type: DataTypes.INTEGER,
      references: {
        model: User,
        key: "user_id",
      },
    },
    date_of_birth: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
  },
  {
    tableName: "families",
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default Family;
