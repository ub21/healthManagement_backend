import { DataTypes } from "sequelize";
import sequelize from "../Config/sequelize.js";
import User from "./User.js";

const ScheduleName = sequelize.define(
  "ScheduleName",
  {
    schedule_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    schedule_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: User,
        key: "user_id",
      },
      allowNull: false,
    }
  },
  {
    tableName: "scheduleName",
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default ScheduleName;
