import { DataTypes } from "sequelize";
import sequelize from "../Config/sequelize.js";
import User from "../Models/User.js";

const UserHealthCondition = sequelize.define(
  "UserHealthCondition",
  {
    user_health_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    health_condition: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
  },
  {
    tableName: "users_health_conditions",
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default UserHealthCondition;
