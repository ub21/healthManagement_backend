import { DataTypes } from "sequelize";
import sequelize from "../Config/sequelize.js";
import Reminder from "./Reminder.js";

const ReminderTime = sequelize.define(
  "ReminderTime",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      unique: true,
    },
    reminder_id: {
      type: DataTypes.INTEGER,
      references: {
        model: Reminder,
        key: "id",
      },
    },
    reminderTimes: {
      type: DataTypes.ARRAY(DataTypes.TIME),
      allowNull: false,
      defaultValue: [],
    },
  },
  {
    tableName: "reminder_times",
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default ReminderTime;
