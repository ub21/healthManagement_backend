import { DataTypes } from "sequelize";
import sequelize from "../Config/sequelize.js";
import Reminder from "./Reminder.js";

const Subscription = sequelize.define(
  "Subscription",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    reminder_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: Reminder, 
        key: 'id',
      },
    },
    subscription_type: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    subscription_start_date: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
    subscription_end_date: {
      type: DataTypes.DATEONLY,
      allowNull: true,
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    tableName: "subscriptions",
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default Subscription;
