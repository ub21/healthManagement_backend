import sequelize from "../Config/sequelize.js";
import { DataTypes } from "sequelize";
import User from "./User.js";

const RefreshToken = sequelize.define(
  "RefreshToken",
  {
    token_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      unique: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: User,
        key: "user_id",
      },
    },
    token: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    expiry: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  },

  {
    tableName: "refreshTokens",
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default RefreshToken;
