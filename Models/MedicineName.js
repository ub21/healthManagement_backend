// MedicineName.js
import { DataTypes } from "sequelize";
import sequelize from "../Config/sequelize.js";

const MedicineName = sequelize.define(
  "MedicineName",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    manufacturer_name: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    pack_size_label: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    short_composition1: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    strength1: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    short_composition2: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    strength2: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    composition_strength: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  },
  {
    tableName: "medicine_names",
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default MedicineName;
