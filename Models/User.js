import { DataTypes } from "sequelize";
import sequelize from "../Config/sequelize.js";

const User = sequelize.define(
  "User",
  {
    user_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    user_first_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    user_last_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    user_gender: {
      type: DataTypes.ENUM,
      values: ["Male", "Female"],
      allowNull: false,
    },
    user_date_of_birth: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    user_aadhar: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    user_email: {
      type: DataTypes.STRING,
      lowercase: true,
      unique: true,
      allowNull: false,
    },
    user_password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    user_phone: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    remember_token: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    profile_picture: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  },
  {
    tableName: "users",
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default User;

// (async () => {
//     try {
//         await sequelize.sync({ alter: true });
//         console.log('Table synced successfully');

//         const tables = await sequelize.query(`
//             SELECT table_name
//             FROM information_schema.tables
//             WHERE table_schema = 'public';
//         `, { type: sequelize.QueryTypes.SELECT });

//         console.log('Tables:', tables);
//     } catch (error) {
//         console.error('Error syncing the tables:', error);
//     }
// })();
