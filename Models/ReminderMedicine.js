import { DataTypes } from "sequelize";
import sequelize from "../Config/sequelize.js";

const ReminderMedicine = sequelize.define(
  "ReminderMedicine",
  {
    reminder_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "reminders",
        key: "id",
      },
      onDelete: "CASCADE",
    },
    medicine_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "medicine_names",
        key: "id",
      },
      onDelete: "CASCADE",
    },
  },
  {
    tableName: "reminder_medicines",
    timestamps: false,
  }
);

export default ReminderMedicine;
