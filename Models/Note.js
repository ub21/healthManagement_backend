import { DataTypes } from "sequelize";
import sequelize from "../Config/sequelize.js";
import User from "./User.js";

const Note = sequelize.define(
  "Note",
  {
    note_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    note_content: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    label_id: {
      type: DataTypes.ENUM(
        "General",
        "Allergy",
        "Mood",
        "Emergency",
        "Side Effects"
      ),
      allowNull: false,
    },
    note_date: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: User,
        key: "user_id",
      },
    },
  },
  {
    tableName: "notes",
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

Note.belongsTo(User, { foreignKey: "user_id" });

export default Note;
