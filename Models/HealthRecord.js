import { DataTypes } from "sequelize";
import sequelize from "../Config/sequelize.js";
import User from "./User.js";

const HealthRecord = sequelize.define(
  "HealthRecord",
  {
    record_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: User,
        key: "user_id",
      },
    },
    record_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    record_type: {
      type: DataTypes.ENUM,
      values: ["Test Results", "Prescription", "Expenses"],
      allowNull: false,
    },
    date_of_prescription: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    file_link: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    doctor_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    facility_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    fees_type: {
      type: DataTypes.ENUM,
      values: ["Doctors fees", "Lab fees", "Medicine Fees"],
      allowNull: true,
    },
  },
  {
    tableName: "healthRecords",
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default HealthRecord;
