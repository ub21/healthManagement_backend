import { DataTypes } from "sequelize";
import sequelize from "../Config/sequelize.js";
import User from "./User.js";

const Vital = sequelize.define(
  "Vitals",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    oxygenSaturation: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    BMI: {
      type: DataTypes.FLOAT,
      allowNull: true,
      unique: false,
    },
    weight: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    height: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    temperature: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    pulse: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    bloodGlucose: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    bloodPressure: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    currentDate: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: User,
        key: "user_id",
      },
    },
  },
  {
    tableName: "vitals",
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default Vital;
