// import Reminder from "./Reminder.js";
// import MedicineName from "./MedicineName.js";
// import ReminderTime from "./ReminderTime.js";
// import sequelize from "../Config/sequelize.js";
// import ReminderMedicine from "./ReminderMedicine.js";
// import ScheduleName from "./scheduleModel.js";
// import Medicine from "./medicineModel.js";
// import User from "./User.js";
// // Associations
// // ScheduleName.hasMany(Medicine, {
// //   foreignKey: 'schedule_id',
// //   sourceKey: 'schedule_id'
// // });
// // Medicine.belongsTo(ScheduleName, {
// //   foreignKey: 'schedule_id',
// //   targetKey: 'schedule_id'
// // });

// // // User and ScheduleName associations
// // User.hasMany(ScheduleName, {
// //   foreignKey: 'user_id',
// //   sourceKey: 'user_id'
// // });
// // ScheduleName.belongsTo(User, {
// //   foreignKey: 'user_id',
// //   targetKey: 'user_id'
// // });

// // // User and ScheduleName associations
// // User.hasMany(ScheduleName, {
// //   foreignKey: 'user_id',
// //   sourceKey: 'user_id'
// // });
// // Associations
// ScheduleName.hasMany(Medicine, {
//   foreignKey: 'schedule_id',
//   sourceKey: 'schedule_id',
//   as: 'medicines'
// });
// Medicine.belongsTo(ScheduleName, {
//   foreignKey: 'schedule_id',
//   targetKey: 'schedule_id'
// });

// // User and ScheduleName associations
// User.hasMany(ScheduleName, {
//   foreignKey: 'user_id',
//   sourceKey: 'user_id'
// });
// ScheduleName.belongsTo(User, {
//   foreignKey: 'user_id',
//   targetKey: 'user_id'
// });

// Reminder.belongsToMany(MedicineName, {
//   through: ReminderMedicine,
//   foreignKey: "reminder_id",
//   otherKey: "medicine_id",
//   as: "Medicines",
// });
// MedicineName.belongsToMany(Reminder, {
//   through: ReminderMedicine,
//   foreignKey: "medicine_id",
//   otherKey: "reminder_id",
//   as: "Reminders",
// });

// ReminderTime.belongsTo(Reminder, {
//   foreignKey: "reminder_id",
//   as: "Reminder",
// });

// Reminder.hasMany(ReminderTime, {
//   foreignKey: "reminder_id",
//   as: "ReminderTimes",
// });

// // Export models and sequelize instance
// const Index = {
//   Reminder,
//   MedicineName,
//   ReminderTime,
//   sequelize,
//   ReminderMedicine,
// };

// export default Index;

import Reminder from "./Reminder.js";
import MedicineName from "./MedicineName.js";
import ReminderTime from "./ReminderTime.js";
import sequelize from "../Config/sequelize.js";
import ReminderMedicine from "./ReminderMedicine.js";
import ScheduleName from "./scheduleModel.js";
import Medicine from "./medicineModel.js";
import User from "./User.js";

// Associations
ScheduleName.hasMany(Medicine, {
  foreignKey: 'schedule_id',
  sourceKey: 'schedule_id',
  as: 'medicines'
});
Medicine.belongsTo(ScheduleName, {
  foreignKey: 'schedule_id',
  targetKey: 'schedule_id'
});

// User and ScheduleName associations
User.hasMany(ScheduleName, {
  foreignKey: 'user_id',
  sourceKey: 'user_id'
});
ScheduleName.belongsTo(User, {
  foreignKey: 'user_id',
  targetKey: 'user_id'
});

Reminder.belongsToMany(MedicineName, {
  through: ReminderMedicine,
  foreignKey: "reminder_id",
  otherKey: "medicine_id",
  as: "Medicines",
});
MedicineName.belongsToMany(Reminder, {
  through: ReminderMedicine,
  foreignKey: "medicine_id",
  otherKey: "reminder_id",
  as: "Reminders",
});

ReminderTime.belongsTo(Reminder, {
  foreignKey: "reminder_id",
  as: "Reminder",
});

Reminder.hasMany(ReminderTime, {
  foreignKey: "reminder_id",
  as: "ReminderTimes",
});

// Export models and sequelize instance
const Index = {
  Reminder,
  MedicineName,
  ReminderTime,
  sequelize,
  ReminderMedicine,
  ScheduleName,
  Medicine,
  User
};

export default Index;
