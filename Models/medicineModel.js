import { DataTypes } from "sequelize";
import sequelize from "../Config/sequelize.js";
import User from "./User.js";
import ScheduleName from "./scheduleModel.js";
const Medicine = sequelize.define(
  "medicines",
  {
    medicine_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    schedule_id: {
      type: DataTypes.INTEGER,
      references: {
        model: ScheduleName, // Use the table name as a string to avoid circular dependency
        key: "schedule_id",
      },
      allowNull: false,
    },
    medicine: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    medicine_type: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    dose: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    instruction: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    schedule: {
      type: DataTypes.ENUM(
        "everyday",
        "specific day of the week",
        "no_of_days"
      ),
    },
    days: {
      type: DataTypes.ARRAY(DataTypes.STRING),
      allowNull: true,
    },
    no_of_days: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    times: {
      type: DataTypes.ARRAY(DataTypes.STRING),
      allowNull: false,
    },
    start_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    end_date: {
      type: DataTypes.DATE,
      allowNull: true,
      validate: {
        isDate: true,
      },
    },
    status: {
      type: DataTypes.ENUM("active", "inactive"),
      defaultValue: "active",
    },
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: User,
        key: "user_id",
      },
      onDelete: "CASCADE",
      onUpdate: "CASCADE",
    },
  },
  {
    tableName: "medicines",
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default Medicine;
