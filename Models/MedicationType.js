import { DataTypes } from "sequelize";
import sequelize from "../Config/sequelize.js";

const MedicationType = sequelize.define(
  "MedicationType",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    medication_type: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
  },
  {
    tableName: "medication_types",
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

// change the table name medication type

export default MedicationType;
