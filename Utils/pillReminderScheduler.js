import cron from "node-cron";
import { Op } from "sequelize";
import ReminderTime from "../Models/ReminderTime.js";

export class PillReminderScheduler {
  constructor() {
    this.scheduleReminder = this.scheduleReminder.bind(this);
  }

  async scheduleReminder() {
    const now = new Date();
    const currentHour = now.getHours();
    const currentMinute = now.getMinutes();
    const currentSecond = now.getSeconds();

    try {
      const reminderTimes = await ReminderTime.findAll({
        where: {
          reminderTimes: {
            [Op.contains]: [`${currentHour}:${currentMinute}:${currentSecond}`],
          },
        },
        attributes: ["reminder_id", "reminderTimes"],
      });

      reminderTimes.forEach(async (reminder) => {
        const { reminder_id, reminderTimes } = reminder;
        console.log(
          `Scheduling reminder for id ${reminder_id}:`,
          reminderTimes
        );

        // Schedule the reminder for each time
        reminderTimes.forEach((time) => {
          const [hour, minute, second] = time.split(":");
          cron.schedule(`${second} ${minute} ${hour} * * *`, () => {
            console.log(`Reminder for id ${id} triggered at ${time}`);
            // Add your reminder logic here, like sending notifications
          });
        });
      });
    } catch (error) {
      console.error("Error fetching or updating reminders:", error);
    }
  }

  startScheduler() {
    cron.schedule("* * * * *", this.scheduleReminder);
  }
}

export default PillReminderScheduler;
