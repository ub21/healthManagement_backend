import push from 'web-push';
import dotenv from 'dotenv';
import Reminder from '../Models/Reminder.js';

// Load environment variables
dotenv.config();

console.log('PUBLIC_KEY:', process.env.PUBLIC_KEY); 
console.log('PRIVATE_KEY:', process.env.PRIVATE_KEY);
// Set VAPID keys for push notifications

const vapiKeys = {
    publicKey: process.env.PUBLIC_KEY,
    privateKey: process.env.PRIVATE_KEY
};

if (!vapiKeys.publicKey || !vapiKeys.privateKey) {
    throw new Error('VAPID keys are not set in the environment variables.');
}

push.setVapidDetails('mailto:bhartiurvashi21@gmail.com', vapiKeys.publicKey, vapiKeys.privateKey);

// Function to fetch a reminder by ID and send a notification
async function sendReminderNotification(reminderId) {
    try {
        // Fetch the reminder details from the database
        const reminder = await Reminder.findByPk(reminderId);

        if (!reminder) {
            console.error(`Reminder with ID ${reminderId} not found`);
            return;
        }

        // Extract message and subscription details
        const message = reminder.message;
        const subscription = reminder.subscription;

        // Send the push notification
        await push.sendNotification(subscription, message);
        console.log(`Notification sent for reminder ID ${reminderId}`);

    } catch (err) {
        console.error('Error sending reminder notification:', err);
    }
}

// Call the function with a specific reminder ID for testing
sendReminderNotification(2);