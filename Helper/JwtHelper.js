import createHttpError from "http-errors";
import JWT from "jsonwebtoken";

class JwtHelper {
  static signAccessToken(user_id) {
    return new Promise((resolve, reject) => {
      const payload = {};
      const secret = process.env.ACCESS_TOKEN_SECRET;
      const options = {
        expiresIn: "1h",
        issuer: "www.tecraki.io",
        audience: user_id.toString(),
      };

      JWT.sign(payload, secret, options, (err, token) => {
        if (err) {
          return reject(err);
        }
        resolve(token);
      });
    });
  }

  static verifyAccessToken(req, res, next) {
    if (!req.headers["authorization"]) {
      return next(
        createHttpError.Unauthorized("Authorization header is missing")
      );
    }

    const authHeader = req.headers["authorization"];
    const bearerToken = authHeader.split(" ");

    if (bearerToken.length !== 2 || bearerToken[0] !== "Bearer") {
      return next(createHttpError.Unauthorized("Invalid authorization format"));
    }

    const token = bearerToken[1];

    JWT.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, payload) => {
      if (err) {
        const message =
          err.name === "JsonWebTokenError" ? "Unauthorized" : err.message;
        return next(createHttpError.Unauthorized(message));
      }
      req.payload = payload;
      next();
    });
  }

  // method for refresh token

  static signRefreshToken(user_id) {
    return new Promise((resolve, reject) => {
      const payload = {};
      const secret = process.env.REFRESH_TOKEN_SECRET;
      const options = {
        expiresIn: "1y",
        issuer: "www.tecraki.io",
        audience: user_id.toString(),
      };

      JWT.sign(payload, secret, options, (err, token) => {
        if (err) {
          return reject(err);
        }
        resolve(token);
      });
    });
  }

  // method for verifying refresh token

  static verifyRefreshToken(refresh_token) {
    return new Promise((resolve, reject) => {
      JWT.verify(
        refresh_token,
        process.env.REFRESH_TOKEN_SECRET,
        (err, payload) => {
          if (err) {
            return reject(
              createHttpError.Unauthorized("Invalid refresh token")
            );
          }
          const user_id = payload.aud;
          resolve(user_id);
        }
      );
    });
  }
}

export default JwtHelper;
