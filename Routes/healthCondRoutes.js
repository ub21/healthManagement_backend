import healthConditionController from "../Controllers/healthConditionController.js";

import express from "express";

const healthCondRoutes = express.Router();

// api for getting the heath conditions

healthCondRoutes.get("/", healthConditionController.getHealthCnditions);

export default healthCondRoutes;
