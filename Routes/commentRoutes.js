import commentController from "../Controllers/commentController.js";

import express from "express";

const commentRoutes = express.Router();

commentRoutes.post("/comment", commentController.saveComment);

commentRoutes.get("/getAllComments/:id", commentController.getComments);

export default commentRoutes;
