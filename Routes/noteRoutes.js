import noteController from "../Controllers/noteController.js";
import express from "express";
import JwtHelper from "../Helper/JwtHelper.js";
const noteRoutes = express.Router();

// route for saving the notes

noteRoutes.post(
  "/saveNote",
  JwtHelper.verifyAccessToken,
  noteController.saveNotes
);

// route for getting the notes with respect to user id

noteRoutes.get(
  "/getNotes/:id",
  JwtHelper.verifyAccessToken,
  noteController.getAllNotes
);

// route for deleting the notes with respect to note id

noteRoutes.delete("/deleteNote/:id", noteController.deleteNote);

export default noteRoutes;
