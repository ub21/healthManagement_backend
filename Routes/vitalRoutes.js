import vitalController from "../Controllers/vitalController.js";
import express from "express";

const vitalRoutes = express.Router();

// api for saving vital routes

vitalRoutes.post("/saveVitals", vitalController.saveVitals);

// api for getting lates vitals

vitalRoutes.get("/getLatest/:userId", vitalController.getLatestVitals);

export default vitalRoutes;
