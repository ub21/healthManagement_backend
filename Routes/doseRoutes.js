import doseController from "../Controllers/doseController.js";
import express from "express";

const doseRoutes = express.Router();

// api for saving routes

doseRoutes.post("/save", doseController.saveDose);

export default doseRoutes;
