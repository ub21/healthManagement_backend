import healthController from "../Controllers/healthController.js";
import express from "express";
import JwtHelper from "../Helper/JwtHelper.js";

const healthRecordRoutes = express.Router();

// api for saving the health record

healthRecordRoutes.post(
  "/saveHealthRecord",
  JwtHelper.verifyAccessToken,
  healthController.addHealthRecord
);

// api for getting the health record

healthRecordRoutes.get(
  "/:user_id",
  JwtHelper.verifyAccessToken,
  healthController.getHealthRecords
);

// api for getting health records by id

healthRecordRoutes.get("/:id", healthController.getRecordById);

export default healthRecordRoutes;
