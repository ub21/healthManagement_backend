import express from "express";
import JwtHelper from "../Helper/JwtHelper.js";
import SubscriptionController from "../Controllers/subscriptionController.js";

const subscriptionRoutes = express.Router();

// Create a new subscription
subscriptionRoutes.post(
  "/createsubscription",
  JwtHelper.verifyAccessToken,
  SubscriptionController.createSubscription
);

// Get subscription by ID
subscriptionRoutes.get(
  "/subscription/:id",
  JwtHelper.verifyAccessToken,
  SubscriptionController.getSubscriptionById
);

// Update a subscription
subscriptionRoutes.put(
  "/subscription/:id",
  JwtHelper.verifyAccessToken,
  SubscriptionController.updateSubscription
);

// Delete a subscription
subscriptionRoutes.delete(
  "/subscription/:id",
  JwtHelper.verifyAccessToken,
  SubscriptionController.deleteSubscription
);

export default subscriptionRoutes;
