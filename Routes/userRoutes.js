import userController from "../Controllers/userController.js";
import express from "express";
import JwtHelper from "../Helper/JwtHelper.js";

const userRoutes = express.Router();

userRoutes.post("/register", userController.createUser);

userRoutes.post("/login", userController.login);

userRoutes.post("/refresh-token", userController.refreshToken);

userRoutes.delete("/logout", userController.logout);

userRoutes.get(
  "/getUser/:user_id",
  JwtHelper.verifyAccessToken,
  userController.getUsers
);

userRoutes.put(
  "/editUser/:user_id",
  JwtHelper.verifyAccessToken,
  userController.editUser
);

export default userRoutes;
