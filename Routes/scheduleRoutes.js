import express from "express";
import { createSchedule , getAllSchedulesByUserId, getScheduleById} from "../Controllers/scheduleController.js";
const router = express.Router();
router.post('/createSchedule', createSchedule);
router.get('/user/:user_id', getAllSchedulesByUserId); // Corrected path
router.get('/schedule/:schedule_id', getScheduleById); // Corrected path
export default router;
