import medicationTypeController from "../Controllers/medicationTypeController.js";

import express from "express";

const medicationTypeRoutes = express.Router();

// getting the type of medicine
medicationTypeRoutes.get("/", medicationTypeController.getMedicineType);

export default medicationTypeRoutes;
