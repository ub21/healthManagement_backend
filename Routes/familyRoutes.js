import familyController from "../Controllers/familyController.js";
import express from "express";
import JwtHelper from "../Helper/JwtHelper.js";

const familyRoutes = express.Router();

// api for saving the family members

familyRoutes.post(
  "/saveFamily",
  JwtHelper.verifyAccessToken,
  familyController.saveFamily
);

// api for getting the family members with respect to patient id

familyRoutes.get(
  "/getFamily/:patientId",
  JwtHelper.verifyAccessToken,
  familyController.getFamilyMembers
);

// api for getting family member detail with respect to id

familyRoutes.get(
  "/getdetails/:familyMemberId",
  familyController.getDetailsById
);

export default familyRoutes;
