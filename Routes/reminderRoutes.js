import reminderController from "../Controllers/reminderController.js";

import express from "express";
import JwtHelper from "../Helper/JwtHelper.js";
// import multer from "multer";
// const upload = multer(); // Initialize multer

const reminderRoutes = express.Router();

//  Create a new reminder

reminderRoutes.post(
  "/savereminder",
  JwtHelper.verifyAccessToken,
  reminderController.saveReminder
);

// getting medicine name with respect to id

reminderRoutes.get("/medicines", reminderController.getMedicineName);

// getting reminder by reminder id

reminderRoutes.get("/reminderByReminderId/:id", reminderController.getRemById);

// getting medication type with respect to id

reminderRoutes.get("/getmedicationtype", reminderController.getMedicineType);

// getting all reminders by user id

reminderRoutes.get(
  "/getRemindersbyId/:user_id",
  JwtHelper.verifyAccessToken,
  reminderController.getReminders
);

// Update an existing reminder

reminderRoutes.put(
  "/editreminder",
  JwtHelper.verifyAccessToken,
  reminderController.editReminder
);

//  Delete an existing reminder
reminderRoutes.delete("/deletereminder/:id", reminderController.deleteReminder);

// search by name

// reminderRoutes.get('/searchByName',reminderController.searchReminder);

// get reminder by user id filtering by current date

reminderRoutes.get(
  "/getRemindersByCurrentdate/:user_id",
  JwtHelper.verifyAccessToken,
  reminderController.getRemindersByUserIdCurrentDate
)

export default reminderRoutes;
