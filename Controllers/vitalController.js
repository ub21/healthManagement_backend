import Vital from "../Models/Vital.js";

export class vitalController {
  //api for saving the vitals
  static async saveVitals(req, res) {
    let {
      bloodPressure,
      bloodGlucose,
      pulse,
      temperature,
      height,
      weight,
      BMI,
      oxygenSaturation,
      currentDate,
      userId,
    } = req.body;

    // Convert empty strings to null
    bloodPressure = bloodPressure === "" ? null : bloodPressure;
    bloodGlucose = bloodGlucose === "" ? null : bloodGlucose;
    pulse = pulse === "" ? null : pulse;
    temperature = temperature === "" ? null : temperature;
    height = height === "" ? null : height;
    weight = weight === "" ? null : weight;
    BMI = BMI === "" ? null : BMI;
    oxygenSaturation = oxygenSaturation === "" ? null : oxygenSaturation;

    try {
      const newVital = await Vital.create({
        bloodPressure,
        bloodGlucose,
        pulse,
        temperature,
        height,
        weight,
        BMI,
        oxygenSaturation,
        currentDate,
        userId,
      });
      return res
        .status(200)
        .json({ message: "vitals added successfully", newVital });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  }

  // api for getting the latest of 7 sets of vitals for the user
  static async getLatestVitals(req, res) {
    try {
      const userId = req.params.userId;

      // Retrieve latest 7 sets of vitals for the user
      const latestVitals = await Vital.findAll({
        where: { userId },
        order: [["currentDate", "DESC"]],
        limit: 7,
      });

      // Format currentDate for each entry and replace nulls with empty strings
      const formattedVitals = latestVitals.map((vital) => {
        const date = new Date(vital.currentDate);
        const formattedDate = `${date.getFullYear()}-${String(
          date.getMonth() + 1
        ).padStart(2, "0")}-${String(date.getDate()).padStart(2, "0")}`;

        // Replace nulls with empty strings
        const formattedVital = {
          ...vital.toJSON(),
          currentDate: formattedDate,
        };
        for (const [key, value] of Object.entries(formattedVital)) {
          if (value === null) {
            formattedVital[key] = "";
          }
        }
        return formattedVital;
      });

      return res.status(200).json({ latestVitals: formattedVitals });
    } catch (error) {
      console.log("Error in getLatestVitals:", error);
      return res.status(500).json({ error: "Internal server error" });
    }
  }
}
export default vitalController;
