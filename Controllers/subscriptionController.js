import Subscription from "../Models/Subscription.js";
import Reminder from "../Models/Reminder.js";

export class SubscriptionController {
  // Create a new subscription
  static async createSubscription(req, res) {
    const {
      user_id,
      reminder_id,
      subscription_type,
      subscription_start_date,
      subscription_end_date,
      status,
    } = req.body;

    try {
      const newSubscription = await Subscription.create({
        user_id,
        reminder_id,
        subscription_type,
        subscription_start_date,
        subscription_end_date,
        status,
      });

      return res.status(201).json({ message: "Subscription created successfully", newSubscription });
    } catch (error) {
      console.error("Error creating subscription:", error);
      return res.status(500).json({ error: "Internal Server Error" });
    }
  }

  // Get subscription by ID
  static async getSubscriptionById(req, res) {
    const { id } = req.params;

    try {
      const subscription = await Subscription.findByPk(id);

      if (!subscription) {
        return res.status(404).json({ error: "Subscription not found" });
      }

      return res.status(200).json(subscription);
    } catch (error) {
      console.error("Error getting subscription:", error);
      return res.status(500).json({ error: "Internal Server Error" });
    }
  }

  // Update a subscription
  static async updateSubscription(req, res) {
    const { id } = req.params;
    const { subscription_type, subscription_start_date, subscription_end_date, status } = req.body;

    try {
      const subscription = await Subscription.findByPk(id);

      if (!subscription) {
        return res.status(404).json({ error: "Subscription not found" });
      }

      await subscription.update({
        subscription_type,
        subscription_start_date,
        subscription_end_date,
        status,
      });

      return res.status(200).json({ message: "Subscription updated successfully", subscription });
    } catch (error) {
      console.error("Error updating subscription:", error);
      return res.status(500).json({ error: "Internal Server Error" });
    }
  }

  // Delete a subscription
  static async deleteSubscription(req, res) {
    const { id } = req.params;

    try {
      const subscription = await Subscription.findByPk(id);

      if (!subscription) {
        return res.status(404).json({ error: "Subscription not found" });
      }

      await subscription.destroy();

      return res.status(200).json({ message: "Subscription deleted successfully" });
    } catch (error) {
      console.error("Error deleting subscription:", error);
      return res.status(500).json({ error: "Internal Server Error" });
    }
  }
}

export default SubscriptionController;
