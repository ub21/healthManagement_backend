import Dose from "../Models/Dose.js";

export class doseController {
  // Method for saving doses
  static async saveDose(req, res) {
    try {
      const { name } = req.body;

      // Check if name is provided
      if (!name) {
        return res.status(400).json({ error: "Name is required" });
      }

      // Save the dose to the database
      const newDose = await Dose.create({ name });

      // Send success response
      return res.status(201).json(newDose);
    } catch (error) {
      // Handle errors
      console.error("Error saving dose:", error);
      return res
        .status(500)
        .json({ error: "An error occurred while saving the dose" });
    }
  }
}

export default doseController;
