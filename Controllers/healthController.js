import HealthRecord from "../Models/HealthRecord.js";
import multer from "multer";
import fs from "fs";
import path from "path";

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "uploads"); // No leading slash
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const upload = multer({
  storage: storage,
});

// check if upload directory exists or not
const uploadDir = "uploads/";

if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir, { recursive: true });
  console.log("uploads directory created");
}

export class healthController {
  // method for adding health record

  static async addHealthRecord(req, res) {
    try {
      upload.single("file_link")(req, res, async function (err) {
        if (err instanceof multer.MulterError) {
          console.error("File upload error:", err);
          return res
            .status(500)
            .json({ error: "File upload error", Error: err });
        } else if (err) {
          console.error("Unknown error:", err);
          return res.status(500).json({ error: "Unknown error", Error: err });
        }

        console.log("Creating health records", req.body);
        console.log("Uploaded file:", req.file);

        // Validate required fields
        const requiredFields = [
          "record_name",
          "record_type",
          "date_of_prescription",
          "doctor_name",
          "facility_name",
          "user_id",
        ];
        for (const field of requiredFields) {
          if (!req.body[field]) {
            console.log(`Missing required field: ${field}`);
            return res
              .status(400)
              .json({ Status: false, error: `${field} is required` });
          }
        }

        try {
          // Create form details
          const healthRecord = await HealthRecord.create({
            record_name: req.body.record_name,
            record_type: req.body.record_type,
            date_of_prescription: req.body.date_of_prescription,
            doctor_name: req.body.doctor_name,
            facility_name: req.body.facility_name,
            fees_type: req.body.fees_type,
            user_id: req.body.user_id,
            file_link: req.file ? req.file.path : null, // Save file path if available
          });

          console.log("Health record saved successfully:", healthRecord);

          // Return success response with the health record data
          return res.json({
            msg: "Health record saved successfully",
            Result: healthRecord,
          });
        } catch (error) {
          console.error("Error during form creation:", error);
          // Delete the uploaded image if form creation fails
          if (req.file) {
            fs.unlink(req.file.path, (err) => {
              if (err) {
                console.error("Error deleting uploaded image:", err);
              }
            });
          }
          return res
            .status(500)
            .json({ error: "Error during record creation", Error: error });
        }
      });
    } catch (error) {
      // Log error message and stack trace
      console.error("Error during saving:", error);

      // Return error response
      return res
        .status(500)
        .json({ error: "Error during saving", Error: error });
    }
  }

  // method for getting all health records by user id

  static async getHealthRecords(req, res) {
    const { user_id } = req.params;

    try {
      // Fetch health records filtered by user_id
      const healthRecords = await HealthRecord.findAll({ where: { user_id } });
      console.log(healthRecords);

      // Process the records to adjust file links
      for (const record of healthRecords) {
        if (record.file_link && fs.existsSync(record.file_link)) {
          // Construct the relative image path for frontend
          const relativeImagePath = path.join(
            "uploads",
            path.basename(record.file_link)
          );
          // Add the relative image path to the health records
          record.file_link = relativeImagePath;
        }
      }

      return res.status(200).json({ Status: true, Result: healthRecords });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ Status: false, Error: error });
    }
  }

  // getting the health record with the id

  static async getRecordById(req, res) {
    try {
      const healthRecord = await HealthRecord.findByPk(req.params.id);
      console.log(healthRecord);

      if (healthRecord) {
        if (healthRecord.file_link && fs.existsSync(healthRecord.file_link)) {
          // Construct the relative image path for frontend
          const relativeImagePath = path.join(
            "uploads",
            path.basename(healthRecord.file_link)
          );

          // Add the relative image path to the health record
          healthRecord.file_link = relativeImagePath;
        }

        return res.status(200).json({ Status: true, Result: healthRecord });
      } else {
        return res
          .status(404)
          .json({ Status: false, Error: "Health record not found" });
      }
    } catch (error) {
      console.log(error);
      return res.status(500).json({ Status: false, Error: error.message });
    }
  }
}

export default healthController;
