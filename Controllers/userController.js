import bcrypt from "bcrypt";
import fs from "fs";
import jwt from "jsonwebtoken";
import multer from "multer";
import User from "../Models/User.js";
import createHttpError from "http-errors";
import JwtHelper from "../Helper/JwtHelper.js";
import RefreshToken from "../Models/RefreshToken.js";

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "profilePicture"); // No leading slash
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const upload = multer({
  storage: storage,
});

const uploadDir = "profilePicture/";

if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir, { recursive: true });
  console.log("uploads directory created");
}

export class userController {
  static async createUser(req, res, next) {
    upload.single("profile_picture")(req, res, async function (err) {
      if (err instanceof multer.MulterError) {
        console.error("File upload error:", err);
        return next(createHttpError(500, "File upload error", { Error: err }));
      } else if (err) {
        console.error("Unknown error:", err);
        return next(createHttpError(500, "Unknown error", { Error: err }));
      }

      console.log("Creating health records", req.body);
      console.log("Uploaded file:", req.file);

      const {
        user_first_name,
        user_last_name,
        user_gender,
        user_date_of_birth,
        user_aadhar,
        user_email,
        user_password,
        user_phone,
      } = req.body;

      if (
        !user_first_name ||
        !user_last_name ||
        !user_gender ||
        !user_date_of_birth ||
        !user_aadhar ||
        !user_email ||
        !user_password ||
        !user_phone
      ) {
        return next(createHttpError.BadRequest("All fields are required"));
      }

      try {
        const existingUser = await User.findOne({ where: { user_email } });
        if (existingUser) {
          return next(
            createHttpError.Conflict(`${user_email} is already registered`)
          );
        }

        const existingPhoneNumber = await User.findOne({where:{user_phone}});
        if(existingPhoneNumber){
          return next(
            createHttpError.Conflict(`${user_phone} is already registered`)
          );
        }

        const hashedPassword = await bcrypt.hash(user_password, 10);
        const newUser = await User.create({
          user_first_name,
          user_last_name,
          user_gender,
          user_date_of_birth,
          user_aadhar,
          user_email,
          user_password: hashedPassword,
          user_phone,
          profile_picture: req.file ? req.file.path : null,
        });

        // Generate JWT token
        let access_token, refresh_token;
        try {
          access_token = await JwtHelper.signAccessToken(newUser.user_id);
          refresh_token = await JwtHelper.signRefreshToken(newUser.user_id);

          // Save refresh token to database
          const expiryDate = new Date();
          expiryDate.setDate(expiryDate.getDate() + 7); // Assuming 7 days expiry for refresh token

          await RefreshToken.create({
            user_id: newUser.user_id,
            token: refresh_token,
            expiry: expiryDate,
          });
        } catch (tokenError) {
          console.error("Token generation error:", tokenError);
          return next(
            createHttpError(500, "Token generation error", {
              Error: tokenError,
            })
          );
        }

        return res
          .status(201)
          .json({
            message: "User added successfully",
            user_id: newUser.user_id,
            access_token,
            refresh_token,
          });
      } catch (error) {
        if (req.file) {
          fs.unlink(req.file.path, (err) => {
            if (err) {
              console.error("Error deleting uploaded image:", err);
            }
          });
        }
        console.error("Error creating user:", error);
        return next(
          createHttpError(500, "Internal Server Error", { Error: error })
        );
      }
    });
  }

  // method for login of the user
  static async login(req, res, next) {
    const { user_email, user_password } = req.body;
    try {
      if (!user_email || !user_password)
        return res
          .status(400)
          .json({ Status: false, error: "Email and password are required" });

      const user = await User.findOne({
        where: {
          user_email,
        },
      });

      if (!user) {
        return next(createHttpError.NotFound("User not registered"));
      }

      // Check password match
      const isMatch = await bcrypt.compare(user_password, user.user_password);

      if (!isMatch) {
        return next(createHttpError.Unauthorized("Password is not valid"));
      }

      // JWT token for authentication
      const access_token = await JwtHelper.signAccessToken(user.user_id);
      const refresh_token = await JwtHelper.signRefreshToken(user.user_id);

      // Save refresh token to database
      const expiryDate = new Date();
      expiryDate.setDate(expiryDate.getDate() + 7); // Assuming 7 days expiry for refresh token

      await RefreshToken.create({
        user_id: user.user_id,
        token: refresh_token,
        expiry: expiryDate,
      });

      // Return the success response with JWT tokens and user profile information
      return res
        .status(200)
        .json({
          Status: true,
          msg: "Login successful",
          user: {
            user_id: user.user_id,
            user_email: user.user_email,
            profile_picture: user.profile_picture,
          },
          access_token,
          refresh_token,
        });
    } catch (error) {
      console.error("Error logging in user:", error);
      return next(
        createHttpError(500, "Internal Server Error", { Error: error })
      );
    }
  }

  // method for refresh token
  static async refreshToken(req, res, next) {
    try {
      const { refresh_token } = req.body;
      if (!refresh_token)
        return next(createHttpError.BadRequest("Refresh token is required"));

      const storedToken = await RefreshToken.findOne({
        where: { token: refresh_token },
      });
      if (!storedToken) {
        return next(createHttpError.Unauthorized("Invalid refresh token"));
      }

      const user_id = await JwtHelper.verifyRefreshToken(refresh_token);
      const access_token = await JwtHelper.signAccessToken(user_id);
      const new_refresh_token = await JwtHelper.signRefreshToken(user_id);

      // Update the refresh token in database
      storedToken.token = new_refresh_token;
      const newExpiryDate = new Date();
      newExpiryDate.setDate(newExpiryDate.getDate() + 7); // Assuming 7 days expiry for refresh token
      storedToken.expiry = newExpiryDate;
      await storedToken.save();

      return res.send({ access_token, refresh_token: new_refresh_token });
    } catch (error) {
      next(error);
    }
  }

  // method for logout of the user
  static async logout(req, res, next) {
    try {
      const { refresh_token } = req.body;
      if (!refresh_token)
        return next(createHttpError.BadRequest("Refresh token is required"));

      // Verify the refresh token
      const user_id = await JwtHelper.verifyRefreshToken(refresh_token);
      if (!user_id) {
        return next(createHttpError.Unauthorized("Invalid refresh token"));
      }

      const storedToken = await RefreshToken.findOne({
        where: { token: refresh_token },
      });
      if (!storedToken) {
        return next(createHttpError.Unauthorized("Invalid refresh token"));
      }

      // Delete the refresh token from the database
      await RefreshToken.destroy({ where: { token: refresh_token } });

      return res.status(200).json({ message: "Logout successful" });
    } catch (error) {
      next(error);
    }
  }

  // method for getting user by user id

  static async getUsers(req, res, next) {
    try {
      const { user_id } = req.params;

      if (!user_id) return next(createHttpError.NotFound("User id required"));

      const user = await User.findOne({
        where: {
          user_id: user_id,
        },
      });
      if (!user) return next(createHttpError.NotFound("User not found"));
      return res.status(200).json({
        Status: true,
        msg: "User details",

        user: {
          user_id: user.user_id,
          user_email: user.user_email,
          profile_picture: user.profile_picture,
          user_first_name: user.user_first_name,
          user_last_name: user.user_last_name,
          user_aadhar: user.user_aadhar,
          user_phone: user.user_phone,
          user_gender: user.user_gender,
          user_date_of_birth: user.user_date_of_birth,
        },
      });
    } catch (error) {
      console.error("Error getting user:", error);
      return next(
        createHttpError(500, "Internal Server Error", { Error: error })
      );
    }
  }

  // method for editting the user

  static async editUser(req, res, next) {
    upload.single("profile_picture")(req, res, async function (err) {
      if (err instanceof multer.MulterError) {
        console.error("File upload error:", err);
        return next(createHttpError(500, "File upload error", { Error: err }));
      } else if (err) {
        console.error("Unknown error:", err);
        return next(createHttpError(500, "Unknown error", { Error: err }));
      }

      const { user_id } = req.params;
      console.log("Requested user id is", user_id);
      console.log("Request body:", req.body);
      console.log("Uploaded file:", req.file);

      try {
        if (!user_id) return next(createHttpError(404, "User ID not provided"));

        const user = await User.findOne({ where: { user_id } });

        if (!user) {
          return next(createHttpError(404, "User not found"));
        }

        const {
          user_first_name,
          user_last_name,
          user_gender,
          user_date_of_birth,
          user_aadhar,
          user_email,
          user_phone,
          user_password,
        } = req.body;

        const updateFields = {};

        if (user_first_name) updateFields.user_first_name = user_first_name;
        if (user_last_name) updateFields.user_last_name = user_last_name;
        if (user_gender) updateFields.user_gender = user_gender;
        if (user_date_of_birth)
          updateFields.user_date_of_birth = user_date_of_birth;
        if (user_aadhar) updateFields.user_aadhar = user_aadhar;
        if (user_email) updateFields.user_email = user_email;
        if (user_phone) updateFields.user_phone = user_phone;
        if (req.file) updateFields.profile_picture = req.file.path;

        // Handle password update
        if (user_password) {
          const hashedPassword = await bcrypt.hash(user_password, 10);
          updateFields.user_password = hashedPassword;
        }

        console.log("Fields to be updated:", updateFields);

        // Update the user
        const [updated] = await User.update(updateFields, {
          where: { user_id },
        });

        if (updated) {
          const updatedUser = await User.findOne({ where: { user_id } });
          return res
            .status(200)
            .json({ message: "User updated successfully", user: updatedUser });
        } else {
          return next(createHttpError(500, "User could not be updated"));
        }
      } catch (error) {
        if (req.file) {
          fs.unlink(req.file.path, (err) => {
            if (err) {
              console.error("Error deleting uploaded image:", err);
            }
          });
        }
        console.error("Error updating user:", error);
        return next(
          createHttpError(500, "Internal Server Error", {
            error: error.message,
          })
        );
      }
    });
  }
}

export default userController;
