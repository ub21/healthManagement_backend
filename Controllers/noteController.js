import Note from "../Models/Note.js";

export class noteController {
  // function to save notes

  static async saveNotes(req, res) {
    const { note_content, note_date, user_id, label_id } = req.body;
    console.log(req.body);

    if (!note_content || !note_date || !user_id || !label_id) {
      const errorMessage = "All fields are required";

      return res.status(400).json(errorMessage);
    }
    // create new note
    try {
      const newNote = await Note.create({
        note_content,
        note_date,
        user_id,
        label_id,
      });
      return res
        .status(200)
        .json({ message: "notes added successfully", newNote });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  }

  // method for getting all notes with respect to the user id
  static async getAllNotes(req, res) {
    try {
      const notes = await Note.findAll({
        where: {
          user_id: req.params.id,
        },
      });

      return res.status(200).json({ notes });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  }

  // function to delete notes with respect to note id

  static async deleteNote(req, res) {
    try {
      const deletedNote = await Note.destroy({
        where: {
          note_id: req.params.id,
        },
      });
      return res.status(200).json({ deletedNote });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  }
}

export default noteController;
