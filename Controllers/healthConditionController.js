import UserHealthCondition from "../Models/UserHeathCondition.js";

export class healthConditionController {
  // function to get the health conditions

  static async getHealthCnditions(req, res) {
    try {
      const healthConditions = await UserHealthCondition.findAll({});
      return res.status(200).json(healthConditions);
    } catch (err) {
      return res.status(500).json(err);
    }
  }
}

export default healthConditionController;
