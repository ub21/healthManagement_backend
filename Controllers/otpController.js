import Otp from "../Models/Otp.js";
import dotenv from "dotenv";
import axios from "axios";
import User from "../Models/User.js";
import { Op } from "sequelize";
import createHttpError from "http-errors";
import JwtHelper from "../Helper/JwtHelper.js";
import RefreshToken from "../Models/RefreshToken.js";

dotenv.config();

const API_KEY_OTP = process.env.API_KEY_OTP;

class otpController {
  static async sendOtp(req, res) {
    try {
      const { phone_number } = req.body;

      // Ensure phone_number is provided
      if (!phone_number) {
        return res
          .status(400)
          .json({ success: false, message: "Phone number is required" });
      }
      // Generate OTP with digits only
      const otp = Math.floor(100000 + Math.random() * 900000);
      console.log(otp);
      // const otp = await otpController.generateNumericOTP(4);

      // Save OTP data to the database
      const otpRecord = await Otp.create({
        phone_number,
        generated_otp: otp,
        otp_expiration: new Date(new Date().getTime() + 10 * 60 * 1000), // Set expiration to 10 minutes from now
      });

      // Send OTP via API
      try {
        const message = `Welcome to the BaelHealth App , Your OTp for Login is  ${otp}. Please don't reply to this message`;
        const response = await axios.post(
          `https://teleos.in/sms-login/services/send.php`,
          null,
          {
            params: {
              key: API_KEY_OTP,
              number: `+91${phone_number}`,
              message: message,
              option: 1,
              type: "sms",
              useRandomDevice: 1,
              prioritize: 1,
            },
          }
        );

        // Check response structure

        if (response.data && response.data.success) {
          return res
            .status(200)
            .json({ success: true, message: "OTP sent successfully" });
        } else {
          console.log("Response:", response.data);
          return res
            .status(400)
            .json({ success: false, message: "Failed to send OTP" });
        }
      } catch (error) {
        console.error("Error sending OTP:", error);
        return res
          .status(500)
          .json({ success: false, message: "Error sending OTP" });
      }
    } catch (error) {
      console.error("Error sending OTP:", error);
      return res
        .status(500)
        .json({ success: false, message: "Error sending OTP" });
    }
  }

  // static async verifyOtp(req, res) {
  //   try {
  //     const { phone_number, generated_otp } = req.body;

  //     // Ensure phone_number and generated_otp are provided
  //     if (!phone_number || !generated_otp) {
  //       return res.status(400).json({
  //         success: false,
  //         message: "Phone number and OTP are required",
  //       });
  //     }

  //     // Find OTP record in the database
  //     const otpData = await Otp.findOne({
  //       where: {
  //         phone_number,
  //         generated_otp,
  //         otp_expiration: { [Op.gt]: new Date() }, // Check if OTP is not expired
  //       },
  //     });

  //     if (otpData) {
  //       console.log("OTP verification successful:", otpData.toJSON());
  //       return res.status(200).json({
  //         success: true,
  //         message: "OTP Verified Successfully",
  //       });
  //     } else {
  //       console.log("OTP verification failed");
  //       return res.status(400).json({ success: false, message: "Wrong OTP" });
  //     }
  //   } catch (error) {
  //     console.error("Error verifying OTP:", error);
  //     return res
  //       .status(500)
  //       .json({ success: false, message: "Error verifying OTP" });
  //   }
  // }

  static async verifyOtp(req, res) {
    try {
      const { phone_number, generated_otp } = req.body;
  
      // Ensure phone_number and generated_otp are provided
      if (!phone_number || !generated_otp) {
        return res.status(400).json({
          success: false,
          message: "Phone number and OTP are required",
        });
      }
  
      // Find OTP record in the database
      const otpData = await Otp.findOne({
        where: {
          phone_number,
          generated_otp,
          otp_expiration: { [Op.gt]: new Date() }, // Check if OTP is not expired
        },
      });
  
      if (!otpData) {
        console.log("OTP verification failed: No matching OTP record found or OTP expired");
        return res.status(400).json({ success: false, message: "Wrong OTP" });
      }
  
      // Check if the phone number exists in the User table
      const user = await User.findOne({ where: { user_phone: phone_number } });
      if (user) {
        // JWT token for authentication
        const access_token = await JwtHelper.signAccessToken(user.user_id);
        const refresh_token = await JwtHelper.signRefreshToken(user.user_id);
  
        // Save refresh token to database
        const expiryDate = new Date();
        expiryDate.setDate(expiryDate.getDate() + 7); // Assuming 7 days expiry for refresh token
  
        await RefreshToken.create({
          user_id: user.user_id,
          token: refresh_token,
          expiry: expiryDate,
        });
  
        console.log("OTP verification successful and user found:", user.toJSON());
        return res.status(200).json({
          success: true,
          flag: 1,
          message: "OTP Verified Successfully",
          user: {
            user_id: user.user_id,
            user_email: user.user_email,
            first_name: user.first_name,
            profile_picture: user.profile_picture,
          },
          access_token,
          refresh_token,
        });
      } else {
        console.log("OTP verification successful but user not found");
        return res.status(200).json({
          success: true,
          flag: 2,
          message: "OTP Verified Successfully, User not registered",
        });
      }
    } catch (error) {
      console.error("Error verifying OTP:", error);
      return res.status(500).json({ success: false, message: "Error verifying OTP" });
    }
  }
  
  
}

export default otpController;
