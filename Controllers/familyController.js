import Family from "../Models/Family.js";
import multer from "multer";
import fs from "fs";
import path from "path";

import moment from "moment";

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    const uploadDir = "profilePhoto/";

    // Create the directory if it doesn't exist
    if (!fs.existsSync(uploadDir)) {
      fs.mkdirSync(uploadDir, { recursive: true });
      console.log("Uploads directory created");
    }

    cb(null, uploadDir);
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const upload = multer({
  storage: storage,
});

export class familyController {
  // api for saving family member details
  static async saveFamily(req, res) {
    upload.single("profile_photo")(req, res, async function (err) {
      if (err instanceof multer.MulterError) {
        console.error("File upload error:", err);
        return res.status(500).json({ error: "File upload error", Error: err });
      } else if (err) {
        console.error("Unknown error:", err);
        return res.status(500).json({ error: "Unknown error", Error: err });
      }

      console.log("Creating family member details", req.body);
      console.log("Uploaded file:", req.file);

      const {
        name,
        relation,
        customRelation,
        gender,
        aadharNumber,
        patient_id,
        date_of_birth,
      } = req.body;

      if (!name || !gender || !aadharNumber || !patient_id || !date_of_birth) {
        return res.status(400).json({ error: "All fields are required" });
      }

      try {
        let finalRelation, finalCustomRelation;

        if (relation === "Others") {
          if (!customRelation) {
            return res
              .status(400)
              .json({ error: "Custom relation cannot be empty" });
          }
          finalRelation = relation; // Use 'Others' as relation if customRelation is used
          finalCustomRelation = customRelation.trim();
        } else {
          if (
            ![
              "Father",
              "Mother",
              "Son",
              "Daughter",
              "Grandfather",
              "Grandmother",
              "Brother",
              "Sister",
              "Uncle",
              "Aunt",
            ].includes(relation)
          ) {
            return res.status(400).json({ error: "Invalid relation value" });
          }
          finalRelation = relation;
          finalCustomRelation = null; // Nullify customRelation if a predefined relation is selected
        }

        // Parse date using moment.js
        const parsedDateOfBirth = moment(date_of_birth, "MMM-DD-YYYY").format(
          "YYYY-MM-DD"
        );

        const familyMember = await Family.create({
          name,
          relation: finalRelation,
          customRelation: finalCustomRelation,
          gender,
          aadharNumber,
          date_of_birth: parsedDateOfBirth,
          profile_photo: req.file ? req.file.path : null,
          patient_id,
        });

        return res
          .status(201)
          .json({ message: "Family member added successfully", familyMember });
      } catch (error) {
        console.error("Error creating family member:", error);

        if (req.file) {
          fs.unlink(req.file.path, (err) => {
            if (err) {
              console.error("Error deleting uploaded image:", err);
            }
          });
        }
        return res.status(500).json({ error: "Internal Server Error" });
      }
    });
  }

  // method for getting all family member details with respect to user id

  static async getFamilyMembers(req, res) {
    try {
      const patient_id = req.params.patientId;
      const familyMembers = await Family.findAll({
        where: {
          patient_id: patient_id,
        },
      });
      return res.status(200).json({ familyMembers });
    } catch (error) {
      console.error("Error getting family members:", error);
      return res.status(500).json({ error: "Internal Server Error" });
    }
  }

  // method for getting family member details with respect to family member id

  static async getDetailsById(req, res) {
    try {
      const familyMemberId = req.params.familyMemberId;

      const familyMember = await Family.findByPk(familyMemberId);

      if (!familyMember) {
        return res.status(404).json({ error: "Family member not found" });
      }

      return res
        .status(200)
        .json({ msg: "Details of a family member", familyMember });
    } catch (error) {
      console.error(error); // Log the error for debugging
      return res
        .status(500)
        .json({ error: "Error getting family member details" });
    }
  }
}

export default familyController;
