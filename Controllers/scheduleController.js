import ScheduleName from '../Models/scheduleModel.js';
import Medicine from '../Models/medicineModel.js';
import sequelize from '../Config/sequelize.js';

export const createSchedule = async (req, res) => {
  const { schedule_name, user_id, medicines } = req.body;
  try {
    // Start a transaction
    const result = await sequelize.transaction(async (t) => {
      // Create a new schedule
      const schedule = await ScheduleName.create(
        {
          schedule_name,
          user_id,
        },
        { transaction: t }
      );

      // Add medicines associated with the schedule
      const medicineRecords = await Medicine.bulkCreate(
        medicines.map((med) => ({
          ...med,
          schedule_id: schedule.schedule_id,
          user_id: user_id,
        })),
        { transaction: t }
      );

      return { schedule, medicineRecords };
    });

    res.status(201).json({
      message: 'Schedule created successfully',
      schedule: result.schedule,
      medicines: result.medicineRecords,
    });
  } catch (error) {
    console.error('Error creating schedule:', error);
    res.status(500).json({ message: 'Failed to create schedule' });
  }
};

export const getAllSchedulesByUserId = async (req, res) => {
    const { user_id } = req.params;
  
    try {
      const schedules = await ScheduleName.findAll({
        where: { user_id },
        include: [
          {
            model: Medicine,
            as: 'medicines'
          }
        ]
      });
  
      res.status(200).json(schedules);
    } catch (error) {
      console.error('Error fetching schedules:', error);
      res.status(500).json({ message: 'Failed to fetch schedules' });
    }
  };
  
  export const getScheduleById = async (req, res) => {
    const { schedule_id } = req.params;
  
    try {
      const schedule = await ScheduleName.findOne({
        where: { schedule_id },
        include: [
          {
            model: Medicine,
            as: 'medicines'
          }
        ]
      });
  
      if (!schedule) {
        return res.status(404).json({ message: 'Schedule not found' });
      }
  
      res.status(200).json(schedule);
    } catch (error) {
      console.error('Error fetching schedule:', error);
      res.status(500).json({ message: 'Failed to fetch schedule' });
    }
  };