import { parse } from "date-fns";
import Reminder from "../Models/Reminder.js";
import ReminderTime from "../Models/ReminderTime.js";
import MedicineName from "../Models/MedicineName.js";
import MedicationType from "../Models/MedicationType.js";
import UserHealthCondition from "../Models/UserHeathCondition.js";
import ReminderMedicine from "../Models/ReminderMedicine.js";
import sequelize from "../Config/sequelize.js";
import index from "../Models/Index.js";

import { Op } from "sequelize";

export class ReminderController {
  static async saveReminder(req, res) {
    try {
      const { medicines } = req.body;

      console.log("Request body:", req.body);

      if (!medicines || medicines.length === 0) {
        console.log("No medicines found in the request.");
        return res.status(400).json({ error: "medicines array is required" });
      }

      const validationErrors = [];

      const reminders = await sequelize.transaction(async (t) => {
        const newReminders = [];

        for (const medicine of medicines) {
          console.log("Processing medicine:", medicine);

          const {
            user_id,
            schedule_name,
            medicine_id,
            medication_type_id,
            instructions,
            medication_schedule,
            dose,
            medication_duration_days,
            medication_start_date,
            medication_end_date,
            reminderTimes,
            medication_schedule_specific_week,
            no_of_days,
            
          } = medicine;

          const existingMedicine = await MedicineName.findOne({
            where: { id: medicine_id }
          });

          if (!existingMedicine) {
            console.log(`Medicine not found: ID ${medicine_id}`);
            validationErrors.push({ error: `Medicine not found: ID ${medicine_id}` });
            continue;
          }

          const newReminder = await Reminder.create({
            user_id,
            schedule_name,
            medication_type_id,
            instructions,
            medication_schedule,
            dose,
            medication_duration_days,
            medication_start_date,
            medication_end_date,
            medication_schedule_specific_week,
            no_of_days,
          }, { transaction: t });

          console.log("Reminder created:", newReminder);
          console.log("no_of_days:", no_of_days);

          await ReminderMedicine.create({
            reminder_id: newReminder.id,
            medicine_id: existingMedicine.id,
            instruction: instructions || "",
            medication_schedule: medication_schedule || "",
            dose: dose || "",
            medication_duration_days: medication_duration_days || 0,
            medication_start_date: medication_start_date || null,
            medication_end_date: medication_end_date || null,
            no_of_days: no_of_days || 0,
            medication_type_id: medication_type_id || ""
          }, { transaction: t });

          if (reminderTimes && reminderTimes.length > 0) {
            const reminderTimesData = reminderTimes.map((time) => ({
              reminder_id: newReminder.id,
              reminderTimes: [time],
            }));
            await ReminderTime.bulkCreate(reminderTimesData, { transaction: t });
          }
    
          newReminders.push(newReminder);
        }

        return newReminders;
      });

      if (validationErrors.length > 0) {
        console.log("Validation errors:", validationErrors);
        return res.status(400).json(validationErrors);
      }

      console.log("Reminders saved successfully.");
      return res.status(201).json({ message: "Reminders created successfully", reminders });
    } catch (error) {
      console.error("Error saving reminder:", error);
      return res.status(500).json({ error: "Internal Server Error" });
    }
  }
  
  
  // get medicine name table with respect to the input
  static async getMedicineName(req, res) {
    const { search } = req.query;
    const limit = parseInt(req.query.limit, 10) || 10;

    if (!search || search.trim() === "") {
      return res.status(400).json({ error: "Search query is required" });
    }

    try {
      // Perform the search with case-insensitive matching and limit the results
      const medicines = await MedicineName.findAll({
        where: {
          name: {
            [Op.iLike]: `${search}%`,
          },
        },
        limit,
        attributes: ["id", "name", "pack_size_label"],
      });

      if (!medicines || medicines.length === 0) {
        return res
          .status(204)
          .json({ error: "No medicines found for the given search query" });
      }

      return res.status(200).json({
        medicines,
      });
    } catch (error) {
      console.error("Error getting medicines:", error);
      return res.status(500).json({ error: "Internal Server Error" });
    }
  }

  // method to get medication type name

  static async getMedicineType(req, res) {
    try {
      const medicine_types = await MedicationType.findAll();
      console.log("medicine_types:", medicine_types);
      if (!medicine_types || medicine_types.length === 0) {
        return res.status(204).json({ error: "No medication types found" });
      }
      return res.status(200).json({ medicine_types });
    } catch (error) {
      console.error("Error getting medicine_types:", error);
      return res.status(500).json({ error: "Internal Server Error" });
    }
  }

  // getting all reminders by specific user id
  static async getReminders(req, res) {
    const { user_id } = req.params;

    console.log("Requested user_id:", user_id);

    try {
        const reminders = await Reminder.findAll({
            where: { user_id },
            include: [
                {
                    model: MedicineName,
                    as: "Medicines",
                    attributes: ["name", "pack_size_label"],
                    through: { attributes: [] }, 
                },
                {
                    model: ReminderTime,
                    as: "ReminderTimes",
                    attributes: ["reminderTimes"], 
                },
            ],
        });

        console.log("Reminders:", reminders);

        if (!reminders || reminders.length === 0) {
            return res.status(204).json({ error: "No reminders found" });
        }

        // Group reminders by schedule_name
        const groupedReminders = reminders.reduce((acc, reminder) => {
            const schedule_name = reminder.schedule_name;
            if (!acc[schedule_name]) {
                acc[schedule_name] = {
                  schedule_name: schedule_name,
                    id: reminder.id.toString(),
                    medicine: []
                };
            }

            acc[schedule_name].medicine.push({
                medicineName: reminder.Medicines[0].name,
                medication_type_id: reminder.medication_type_id,
                dose: reminder.dose,
                ReminderTimes: reminder.ReminderTimes.map(rt => rt.reminderTimes).flat(),
                instructions: reminder.instructions,
                medication_schedule: reminder.medication_schedule,
                medication_schedule_specific_week: reminder.medication_schedule_specific_week,
                no_of_days: reminder.no_of_days,
                medication_start_date: reminder.medication_start_date,
                medication_end_date: reminder.medication_end_date,
                status: reminder.status,
                medication_duration_days: reminder.medication_duration_days

            });

            return acc;
        }, {});

        // Converting groupedReminders object to array
        const formattedReminders = Object.values(groupedReminders);

        return res.status(200).json({ reminders: formattedReminders });
    } catch (error) {
        console.error("Error getting reminders:", error);
        return res.status(500).json({ error: "Internal Server Error" });
    }
}

   // edit the reminder function

  static async editReminder(req, res) {
    const {
      id,
      user_id,
      medicine_id,
      medication_type_id,
      user_health_condition,
      custom_health_condition,
      number_of_medication_taken,
      dose,
      medication_schedule,
      medication_schedule_specific_week,
      no_of_days,
      medication_start_date,
      medication_end_date,
      reminderTimes,
      schedule_name,
      instructions,
    } = req.body;

    console.log("Request body:", req.body);

    // Check if id is provided
    if (!id) {
      return res.status(400).json({ error: "reminder id is required" });
    }

    // Check if medication_start_date is provided
    if (!medication_start_date) {
      return res
        .status(400)
        .json({ error: "medication_start_date is required" });
    }

    // Parse dates with 'dd/MM/yyyy' format
    let parsedStartDate,
      parsedEndDate = null;
    try {
      parsedStartDate = parse(medication_start_date, "dd/MM/yyyy", new Date());
      if (medication_end_date) {
        parsedEndDate = parse(medication_end_date, "dd/MM/yyyy", new Date());
      }
    } catch (error) {
      console.error("Error parsing dates:", error);
      return res.status(400).json({ error: "Invalid date format" });
    }

    // Check if the parsed dates are valid
    if (
      isNaN(parsedStartDate.getTime()) ||
      (parsedEndDate && isNaN(parsedEndDate.getTime()))
    ) {
      return res.status(400).json({ error: "Invalid date format" });
    }

    console.log("Parsed start date:", parsedStartDate);
    console.log("Parsed end date:", parsedEndDate);

    // Ensure medication_schedule_specific_week is an array
    let specificWeekArray;
    if (Array.isArray(medication_schedule_specific_week)) {
      specificWeekArray = medication_schedule_specific_week;
    } else if (typeof medication_schedule_specific_week === "string") {
      specificWeekArray = medication_schedule_specific_week
        .split(",")
        .map((day) => day.trim());
    } else {
      return res.status(400).json({
        error: "Invalid format for medication_schedule_specific_week",
      });
    }

    try {
      // Handle custom health condition
      if (user_health_condition === "Others" && custom_health_condition) {
        // Check if the custom health condition already exists
        const existingCondition = await UserHealthCondition.findOne({
          where: { health_condition: custom_health_condition },
        });

        // If it doesn't exist, create it
        if (!existingCondition) {
          await UserHealthCondition.create({
            health_condition: custom_health_condition,
          });
        }
      } else {
        const existingCondition = await UserHealthCondition.findOne({
          where: { health_condition: user_health_condition },
        });

        if (!existingCondition) {
          return res.status(400).json({ error: "Invalid health condition" });
        }
      }

      // Find reminder by id
      const reminder = await Reminder.findByPk(id);
      if (!reminder) {
        return res.status(404).json({ error: "reminder not found" });
      }

      // Update the reminder
      await reminder.update({
        user_id,
        medicine_id,
        medication_type_id,
        user_health_condition:
          user_health_condition === "Others" ? "Others" : user_health_condition,
        custom_health_condition:
          user_health_condition === "Others" ? custom_health_condition : null,
        number_of_medication_taken,
        dose,
        medication_schedule,
        medication_schedule_specific_week: specificWeekArray,
        no_of_days,
        medication_start_date: parsedStartDate,
        medication_end_date: parsedEndDate,
        schedule_name,
        instructions,
      });

      console.log("Updated reminder:", reminder);

      // Update reminder times
      if (Array.isArray(reminderTimes)) {
        // Delete existing reminder times
        await ReminderTime.destroy({ where: { reminder_id: id } });

        // Create new reminder times
        const createdReminderTimes = await Promise.all(
          reminderTimes.map(async (time) => {
            console.log("Processing time:", time);
            return await ReminderTime.create({
              reminder_id: id,
              reminderTimes: [time],
            });
          })
        );

        console.log("Created reminder times", createdReminderTimes);
        return res.status(200).json({
          message: "reminder updated successfully",
          reminder,
          reminderTimes: createdReminderTimes,
        });
      } else {
        console.error("reminder times is not an array:", reminderTimes);
        return res
          .status(400)
          .json({ error: "reminder times should be an array" });
      }
    } catch (error) {
      console.error("Error updating reminder", error);
      return res.status(500).json({ error: "Internal Server Error" });
    }
  }
  // method for deleting the reminder with the reminder times by reminder id

  static async deleteReminder(req, res) {
    const { id } = req.params;
    console.log("requested reminder id ", id);

    try {
      const reminders = await Reminder.findAll({
        where: {
          id,
        },
      });

      if (!reminders || reminders.length === 0) {
        return res.status(401).json({ error: "No reminder set for this id" });
      }

      // get all reminder id's

      const reminderIds = reminders.map((reminder) => reminder.id);

      // delete reminder times associated with these reminders

      await ReminderTime.destroy({
        where: {
          reminder_id: reminderIds,
        },
      });
      // delete the reminders

      await Reminder.destroy({
        where: {
          id,
        },
      });

      console.log("deleted reminders", reminders);
      return res.status(200).json({ message: "reminder deleted successfully" });
    } catch (error) {
      console.error("Error deleting reminder:", error);
      return res.status(500).json({ error: "Reminder deletion error" });
    }
  }

  // get the reminder by reminder id

  static async getRemById(req, res) {
    const { id } = req.params;
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.limit, 10) || 10;
    const offset = (page - 1) * limit;

    console.log("requested id is", id);

    try {
        const reminder = await Reminder.findByPk(id, {
            include: [
                {
                    model: MedicineName,
                    as: "Medicines", // Correct alias based on the error message
                    attributes: ["name", "pack_size_label"],
                },
                {
                    model: ReminderTime,
                    as: "ReminderTimes",
                    attributes: ["reminderTimes"],
                },
            ],
        });

        console.log("reminder:", reminder);

        if (!reminder) {
            return res.status(204).json({ error: "No reminder found" });
        }

        // Pagination logic for reminder times
        const paginatedReminderTimes = reminder.ReminderTimes.slice(offset, offset + limit);

        const formattedReminder = {
            ...reminder.get(),
            medicines: reminder.Medicines.map(medicine => ({
                medicineName: medicine.name,
                medicationTypeName: medicine.pack_size_label
            })),
            reminderTimes: paginatedReminderTimes.map(rt => rt.reminderTimes).flat(), // Extracting reminder times
        };

        return res.status(200).json({
            reminder: formattedReminder,
            currentPage: page,
            totalPages: Math.ceil(reminder.ReminderTimes.length / limit),
        });
    } catch (error) {
        console.error("Error getting reminder:", error);
        return res.status(500).json({ error: "Internal Server Error" });
    }
}


  // method for getting all reminders by user id where search type is current date

  static async getRemindersByUserIdCurrentDate(req,res){

   
      const { user_id } = req.params;
  
      console.log("Requested user_id:", user_id);
  
      try {
          // Get the current date
          const currentDate = new Date();
          const currentDayName = currentDate.toLocaleString('en-US', { weekday: 'long' });
          const formattedCurrentDate = currentDate.toISOString().split('T')[0]; // Format to YYYY-MM-DD
  
          const reminders = await Reminder.findAll({
              where: {
                  user_id,
                  medication_start_date: { [Op.lte]: formattedCurrentDate },
              },
              include: [
                  {
                      model: MedicineName,
                      as: "Medicines",
                      attributes: ["name", "pack_size_label"],
                      through: { attributes: [] }, 
                  },
                  {
                      model: ReminderTime,
                      as: "ReminderTimes",
                      attributes: ["reminderTimes"], 
                  },
              ],
          });
  
          console.log("Reminders:", reminders);
  
          if (!reminders || reminders.length === 0) {
              return res.status(204).json({ error: "No reminders found" });
          }
  
          // Group reminders by schedule_name
          const groupedReminders = reminders.reduce((acc, reminder) => {
              // Check if the current day is included in the medication schedule specific week
              if (!reminder.medication_schedule_specific_week.includes(currentDayName)) {
                  return acc;
              }
  
              const schedule_name = reminder.schedule_name;
              if (!acc[schedule_name]) {
                  acc[schedule_name] = {
                      schedule_name: schedule_name,
                      id: reminder.id.toString(),
                      medicine: []
                  };
              }
  
              acc[schedule_name].medicine.push({
                  medicineName: reminder.Medicines[0].name,
                  medication_type_id: reminder.medication_type_id,
                  dose: reminder.dose,
                  ReminderTimes: reminder.ReminderTimes.map(rt => rt.reminderTimes).flat(),
                  instructions: reminder.instructions,
                  medication_schedule: reminder.medication_schedule,
                  medication_schedule_specific_week: reminder.medication_schedule_specific_week,
                  no_of_days: reminder.no_of_days,
                  medication_start_date: reminder.medication_start_date,
                  medication_end_date: reminder.medication_end_date,
                  status: reminder.status,
                  medication_duration_days: reminder.medication_duration_days
              });
  
              return acc;
          }, {});
  
          // Converting groupedReminders object to array
          const formattedReminders = Object.values(groupedReminders);
  
          return res.status(200).json({ reminders: formattedReminders });
      } catch (error) {
          console.error("Error getting reminders:", error);
          return res.status(500).json({ error: "Internal Server Error" });
      }
  }
  

  }


export default ReminderController;

