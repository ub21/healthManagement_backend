import Comment from "../Models/Comment.js";

export class commentController {
  // method for saving comments

  static async saveComment(req, res) {
    try {
      const { note_id, user_id, content } = req.body;

      if (!note_id || !user_id || !content) {
        return res.status(400).json({
          message: "All fields are required",
        });
      }

      const newComment = await Comment.create({
        note_id,
        user_id,
        content,
      });
      return res.status(201).json({
        message: "Comment added successfully",
        data: newComment,
      });
    } catch (error) {
      return res.status(500).json({
        message: "Internal server error",
      });
    }
  }

  // method for getting all comments with respect to note id

  static async getComments(req, res) {
    try {
      const note_id = req.params.id;

      if (!note_id) {
        return res.status(400).json({
          message: "Note id is required",
        });
      }

      const comments = await Comment.findAll({
        where: {
          note_id,
        },
      });
      return res.status(200).json({
        message: "Comments fetched successfully",
        data: comments,
      });
    } catch (error) {
      return res.status(500).json({
        message: "Internal server error",
      });
    }
  }
}
export default commentController;
