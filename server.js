import express from "express";
import dotenv from "dotenv";
import cors from "cors";
import path from "path";
import morgan from "morgan";
import createError from "http-errors";
import sequelize from "./Config/sequelize.js";
import PillReminderScheduler from "./Utils/pillReminderScheduler.js";
import Subscription from "./Models/Subscription.js";
import userRoutes from "./Routes/userRoutes.js";
import noteRoutes from "./Routes/noteRoutes.js";
import commentRoutes from "./Routes/commentRoutes.js";
import healthCondRoutes from "./Routes/healthCondRoutes.js";
import reminderRoutes from "./Routes/reminderRoutes.js";
import healthRecordRoutes from "./Routes/healthRecordRoutes.js";
import vitalRoutes from "./Routes/vitalRoutes.js";
import familyRoutes from "./Routes/familyRoutes.js";
import doseRoutes from "./Routes/doseRoutes.js";
import otpRoutes from "./Routes/otpRoutes.js";
import subscriptionRoutes from "./Routes/subscriptionRoutes.js";
import bodyParser from "body-parser";

import scheduleRoutes from "./Routes/scheduleRoutes.js";

import JwtHelper from "./Helper/JwtHelper.js";
import Otp from "./Models/Otp.js";

// Load environment variables
//dotenv.config();
const app = express();
const port = process.env.PORT || 5000;
const __dirname = path.resolve();

// Middleware setup
app.use(express.urlencoded({ extended: false }));
// allows to parse the response into json format
app.use(express.json());
app.use(cors());
app.use(morgan("dev"));
app.use(bodyParser.json());

// Static files middleware
app.use("/uploads", express.static(path.join(__dirname, "uploads")));
app.use(
  "/profilePicture",
  express.static(path.join(__dirname, "profilePicture"))
);
app.use("/profilePhoto", express.static(path.join(__dirname, "profilePhoto")));

// Database connection
sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch((error) => {
    console.error("Unable to connect to the database:", error);
    process.exit(1);
  });

// Routes
app.use("/api/user", userRoutes);
app.use("/api/notes", noteRoutes);
app.use("/api/comments", commentRoutes);
app.use("/api/healthCond", healthCondRoutes);
app.use("/api/reminders", reminderRoutes);
app.use("/api/healthRecords", healthRecordRoutes);
app.use("/api/vitals", vitalRoutes);
app.use("/api/familyMember", familyRoutes);
app.use("/api/dose", doseRoutes);
app.use("/api/otp", otpRoutes);
app.use("/api/subscription", subscriptionRoutes);
app.use("/api/schedule", scheduleRoutes);

//Home route
app.get("/", (req, res, next) => {
  res.send("Hello World 123 Tecraki!");
});
// app.use(express.static('build'));

app.get("/home", (req, res) => {
  res.render("home");
});

// 404 handler
app.use((req, res, next) => {
  next(createError.NotFound("This route does not exist"));
});

// Error handler
app.use((err, req, res, next) => {
  if (!err.status) {
    // Handle non-HTTP errors
    console.error("Unhandled error:", err);
    res.status(500).json({ error: "Internal Server Error" });
  } else {
    // Handle HTTP errors
    res.status(err.status);
    res.json({
      error: {
        status: err.status,
        message: err.message,
      },
    });
  }
});
// Sync database tables
(async () => {
  try {
    await sequelize.sync({ alter: true });
    console.log("Table synced successfully");

    const tables = await sequelize.query(
      `
      SELECT table_name
      FROM information_schema.tables
      WHERE table_schema = 'public';
    `,
      { type: sequelize.QueryTypes.SELECT }
    );

    console.log("Tables:", tables);
  } catch (error) {
    console.error("Error syncing the tables:", error);
  }
})();

// Cron job setup
const reminderScheduler = new PillReminderScheduler();
reminderScheduler.startScheduler();

// Start server
app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
